package com.bigfans.searchservice.model;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class ProductSearchRequest implements Serializable{

	private static final long serialVersionUID = -48267493139036305L;
	private String keyword;
	private String tagId;
	private Map<String , String> attrs = new HashMap<>();
	private Map<String , String> sorts = new HashMap<>();
	private String categoryId;
	private String brandId;
	private String minPrice;
	private String maxPrice;

}
