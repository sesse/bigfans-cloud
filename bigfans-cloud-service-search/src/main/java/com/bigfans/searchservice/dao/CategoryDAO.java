package com.bigfans.searchservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.searchservice.model.Category;

public interface CategoryDAO extends BaseDAO<Category> {

	String getParentId(String catId);
}
