package com.bigfans.searchservice.document.convertor;

import com.bigfans.framework.es.DocumentConverter;
import com.bigfans.framework.es.IndexDocument;
import com.bigfans.searchservice.model.Tag;

import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月8日上午9:40:58
 *
 */
public class TagDocumentConverter implements DocumentConverter<Tag> {

	@Override
	public Tag toObject(Map<String, Object> dataMap) {
		Tag tag = new Tag();
		tag.setId((String)dataMap.get("id"));
		tag.setName((String)dataMap.get("name"));
		tag.setRelatedCount((Integer)dataMap.get("related_count"));
		return tag;
	}

	@Override
	public IndexDocument toDocument(Tag obj) {
		IndexDocument doc = new IndexDocument(obj.getId());
		doc.put("id", obj.getId());
		doc.put("name", obj.getName());
		doc.put("related_count", obj.getRelatedCount());
		return doc;
	}

}
