package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class TagMapping extends BaseMapping {

	public static final String INDEX = "tag";
	public static final String TYPE = "Tag";
	public static final String ALIAS = "tag_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_NAME_RAW = "raw";
	public static final String FIELD_NAME_PINYIN = "pinyin";
	public static final String FIELD_RELATED_COUNT = "related_count";
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
									.field("store", true)
								.endObject()
								.startObject(FIELD_NAME)
									.field("type", "text")
									.startObject("fields")
					            		.startObject(FIELD_NAME_RAW)
					            			.field("type", "keyword")
					            		.endObject()
					            		.startObject(FIELD_NAME_PINYIN)
					            			.field("type", "text")  
								            .field("index", true)
								            .field("analyzer", "pinyin")
					            		.endObject()
						            .endObject()
								.endObject()
						        .startObject(FIELD_RELATED_COUNT)  
						            .field("type", "integer")  
						            .field("index", false)
						        .endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
