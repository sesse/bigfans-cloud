package com.bigfans.systemservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class SystemServiceApp {

    public static void main(String[] args){
        SpringApplication.run(SystemServiceApp.class , args);
    }

}
