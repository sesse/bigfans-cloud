package com.bigfans.systemservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.systemservice.model.Operator;

public interface OperatorDAO extends BaseDAO<Operator> {
	
	int countByUsername(String account);
	
}