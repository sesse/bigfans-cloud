package com.bigfans.systemservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="Operator")
public class OperatorEntity extends AbstractModel {

	public String getModule() {
		return "Operator";
	}
	
	private static final long serialVersionUID = -1189566871934067878L;

	public static final int STATUS_ACTIVATED = 1;
	public static final int STATUS_NOT_ACTIVATED = 0;

	public static final int SEX_MAN = 1;
	public static final int SEX_WOMAN = 0;

	@Column(name = "status")
	protected Integer status;
	@Column(name = "username")
	protected String username;
	@Column(name = "password")
	protected String password;
	@Column(name = "email")
	protected String email;
	@Column(name = "reg_date",nullable=false,columnDefinition=COLUMN_TYPE_DATETIME)
	protected Date regDate;
	@Column(name = "reg_type")
	protected String regType;
	@Column(name = "nickname")
	protected String nickname;
	@Column(name = "age")
	protected Integer age;
	@Column(name = "sex")
	protected Integer sex;
	@Column(name = "icon_path")
	protected String iconPath;
}