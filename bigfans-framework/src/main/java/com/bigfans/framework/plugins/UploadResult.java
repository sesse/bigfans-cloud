package com.bigfans.framework.plugins;

import lombok.Data;

@Data
public class UploadResult {

	private boolean success;
	private String storageType;
	private String server;
	private String filePath;
	private String fileKey;

}
