package com.bigfans.orderservice.api.form;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lichong
 * @create 2018-03-15 下午8:44
 **/
@Data
public class OrderCreateItemForm {

    private String prodId;

    private Integer quantity;

    private BigDecimal price;

    private BigDecimal subtotal;

}
