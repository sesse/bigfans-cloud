package com.bigfans.model.dto.order;

import com.bigfans.framework.utils.ArithUtils;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lichong
 * @create 2018-03-13 下午9:58
 **/
@Data
public class OrderItemPricingResultDto {

    private String prodId;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal originalPrice;
    private BigDecimal subTotal;
    private BigDecimal originalSubTotal;
    private String pmtMessage;

    public BigDecimal getSavedMoney(){
        return ArithUtils.sub(originalSubTotal , subTotal);
    }

}
