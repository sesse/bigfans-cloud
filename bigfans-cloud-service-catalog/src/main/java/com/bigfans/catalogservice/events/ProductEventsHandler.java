package com.bigfans.catalogservice.events;

import com.bigfans.framework.event.EventRepository;
import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.ProductCreatedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @author lichong
 * @create 2018-03-17 上午10:13
 **/
@Component
public class ProductEventsHandler {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private EventRepository eventRepository;

    private static final Logger logger = LoggerFactory.getLogger(ProductEventsHandler.class);

    @Transactional
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void on(ProductCreatedEvent event) {
        eventRepository.save(event);
        kafkaTemplate.send(event);
        logger.debug("ProductCreatedEvent invoked");
    }
}
