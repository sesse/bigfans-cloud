package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.catalogservice.service.attribute.AttributeValueService;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @create 2018-05-13 下午12:42
 **/
@RestController
public class AttributeApi extends BaseController {

    @Autowired
    private AttributeValueService attributeValueService;

    @GetMapping(value = "/attributes")
    public RestResponse listByProd(
            @RequestParam(value = "prodId" , required = false) String prodId,
            @RequestParam(value = "ids" , required = false) List<String> ids
    ) throws Exception {
        List<AttributeValue> valueList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(ids)){
            valueList = attributeValueService.listById(ids);
        } else if (StringHelper.isNotEmpty(prodId)){
            valueList = attributeValueService.listByProduct(prodId);
        }
        return RestResponse.ok(valueList);
    }

}
