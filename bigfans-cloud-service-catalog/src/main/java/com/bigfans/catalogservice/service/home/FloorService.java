package com.bigfans.catalogservice.service.home;

import com.bigfans.catalogservice.model.Floor;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年8月8日下午8:23:40
 *
 */
public interface FloorService {
	
	int createFloor(Floor floor) throws Exception;
	
	List<Floor> listFloor() throws Exception ;
	
}
