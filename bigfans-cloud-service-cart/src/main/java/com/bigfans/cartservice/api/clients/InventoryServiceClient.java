package com.bigfans.cartservice.api.clients;

import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.cartservice.CartApplications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-16 下午3:06
 **/
@Component
public class InventoryServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Integer> getSurplusStock(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, CartApplications.getFunctionalUser());
            Integer stock = serviceRequest.get(Integer.class , "http://catalog-service/stock?prodId={prodId}" , prodId);
            return stock;
        });
    }

}
